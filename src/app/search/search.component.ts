import { Component, OnInit } from '@angular/core';
import {SearchService} from '../service/serch.service';
import { Observable, Subject } from 'rxjs';
import {Item, Params} from './interfaces.model';
import {Router, ActivatedRoute} from "@angular/router";
import {MatDialog} from '@angular/material';
import {QuickViewDialogComponent} from '../quick-view-dialog/quick-view-dialog.component';

@Component({
  selector: 'app-search-component',
  providers: [SearchService],
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})

export class SearchComponent implements OnInit {
  ngOnInit() {}

  public items: Item;
  public pages: object;
  public searchText;

  modelChanged: Subject<string> = new Subject<string>();
  pageCahnged: Subject<object> = new Subject<object>();

  currentPage: number;

  pagesListHelper: (n: number) => Number[];

  public counter$: Observable<string> = new Subject<number>()
    .scan((acc: number, current: number): number => acc + current)
    .map((value: number): string => `Sum of clicks: ${value}`)
 
  constructor(private _searchService: SearchService, private router: Router,
    private activeRoute: ActivatedRoute, public dialog: MatDialog) {
    this.pagesListHelper = (n: number): Number[] => {
      let arr:Array<Number>=new Array(n);

      return arr.fill(0).map((undefined, i) => ++i);
    }

    this.activeRoute.params
    .subscribe((params: Params) => this.initialPageRendering(params));

    // this.counter$.subscribe((text) => {
    //   console.log(text);
    // })

    this.modelChanged
    .debounceTime(1000)
    .subscribe(search => search && this.router.navigate(['search', search.replace(/\s/g, '+'), 1]));

    this.pageCahnged
    .subscribe(
      (data: Item) => {
        this.items = data;
        this.currentPage = data['page'];
        this.pages = {
          list: this.pagesListHelper(data['total_pages']),
          current: data['page']
        };
      },
      err => console.error(err),
      () => console.log('done loading films')
    );
  }

  openPage(page) {
    if (page === this.currentPage) {
      return;
    }
    this.router.navigate(['search', this.searchText.replace(/\s/g, '+'), page]);
  }

  getItemsList(searchValue: string): void {
    this.modelChanged.next(searchValue)
  }

  initialPageRendering({page, text}): void {
    if (page && text) {
      this.currentPage = Number(page);
      this.searchText = text.replace(/\+/g, ' ');
      this._searchService.getItems(text, page)
      .subscribe((data) => this.pageCahnged.next(data));
    }
  }

  openDialog(item): void {
    const dialogRef = this.dialog.open(QuickViewDialogComponent, {
      width: '350px',
      data: {
        overview: item.overview,
        release: item.release_date
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result, 'The dialog was closed');
    });
  }

  goToDetailsPage(id) {
    this.router.navigate(['/details-page', id]);
  }
}

//https://codecraft.tv/courses/angular/routing/parameterised-routes/
//https://codecraft.tv/courses/angular/routing/navigation/

// https://stackoverflow.com/questions/36354325/angular-2-ngfor-using-numbers-instead-collections
// https://www.jstips.co/en/javascript/create-range-0...n-easily-using-one-line/

// // const API_URL = '/netflixroulette.net/api/api.php?title=';
// // const API_URL_DIRECTOR = '/netflixroulette.net/api/api.php?director=';
// const testUrl = 'https://api.themoviedb.org/3/search/movie?api_key=40f28f2bdd6c3d98e12196cde3f1aa4c&query=';
// export function setQuery(query) {
//   return {
//     type: SET_QUERY,
//     query
//   }
// }

// export function requestItems() {
//   // const filter = store.getState().search.filter;
//   // const path = (filter === 'director') ? API_URL_DIRECTOR : API_URL;
//   const query = store.getState().query.query;
//   const data = request.get(`${ testUrl }${ query.replace(/\s/g, '+') }`)
//   .then((data) => data.body, () => {data: []});
//   // const data = {};

//   console.log(store.getState().search.filter);

//   return {
//     type: REQUEST_FILMS,
//     payload: data
//   }
// }

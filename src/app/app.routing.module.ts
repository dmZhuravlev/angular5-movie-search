import { RouterModule, Routes } from '@angular/router';
import { ModuleWithProviders } from "@angular/core";

import { SearchComponent } from './search/search.component';
import { MenuComponent } from './menu/menu.component';
import { DetailsPageComponent } from './details-page/details-page.component';

const routes: Routes = [
    {
        path: 'search',
        component: SearchComponent,
        data: {
            breadcrumb: "Search"
        }
    },
    { 
        path: 'search/:text/:page',
        component: SearchComponent,
        data: {
            breadcrumb: "Search"
        }
    },
    { 
        path: 'menu',
        component: MenuComponent,
        data: {
            breadcrumb: "Menu"
        } 
    },
    { 
        path: 'details-page/:id',
        component: DetailsPageComponent,
        data: {
            breadcrumb: "Details"
        }
    },
    { path: '', redirectTo: '/', pathMatch: 'full' },
    { path: '**', redirectTo: '/', pathMatch: 'full' }
    // { path : '', component : AppComponent }
];

  export const routing: ModuleWithProviders = RouterModule.forRoot(routes);
import { Component, OnInit } from '@angular/core';

import {FormBuilder, FormGroup, FormArray, FormControl} from '@angular/forms';

@Component({
  selector: 'app-menu-component',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent {
  // name = new FormControl('');
  personForm: FormGroup;
  nameControl: FormControl;
  formSubmitted: boolean;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.nameControl = new FormControl('TestName');

    this.personForm = this.fb.group({
      name: this.nameControl,
      age: '',
      address: this.fb.group({
        country: '',
        city: 'Oakland'
      }),
      phones: this.fb.array([
        '2222222',
        '1111111'
      ])
    });
    
    this.personForm.valueChanges.subscribe(() => {
      this.formSubmitted = false;
    });
  }

  get phonesArrayControl(): FormArray {
    return <FormArray>this.personForm.get('phones');
  }
  
  addPhone() {
    this.phonesArrayControl.push(new FormControl(''));
  }
  
  removePhone(index: number) {
    this.phonesArrayControl.removeAt(index);
  }
  
  submit(event: Event) {
    event.preventDefault();
    this.formSubmitted = true;
  }
}
